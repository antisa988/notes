<?php
    include "db.php";

    if(isset($_POST['submit'])){
        $title = $_POST['opis'];
        $sql = "INSERT INTO `biljeska`(`opis`) VALUES ('$title')";
        $res = mysqli_query($conn,$sql);
    }

$ress=mysqli_query($conn,"select * from biljeska");
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Notes</title>
    <link href="/css/style.css" rel="stylesheet" type="text/css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://code.iconify.design/2/2.2.1/iconify.min.js"></script>
    <link rel="stylesheet" href="/font-awesome/css/font-awesome.min.css">
    <style type="text/css">

    input{
        position: relative;
        display: block;
        float: right;
    }
    .msg{
        display: flex;
        position: relative;
        float: right;
    }
    .card-body{
        margin-bottom: 20px;

    }
    </style>

</head>
<body>

<div style="margin-left: 50px;">
<h1>Notes App</h1>
<br>
<h2>Add new note</h2>
</div>
<div class="container-my-4" style="width: 75%; margin-top:50px; margin-left: 50px">
    <div class="row justify content-center">
        <div class="col-lg-10">
            <form class="form" method="POST">
                <div class="card my-3">

                    <input type="text" class="form-control" id="opis" name="opis">
                <button type="submit" name="submit" class="btn btn-primary" style="width: 40px; position:absolute;  left:101%; float: right;">+</button>

            </form>
        </div>
    </div>
</div>

<div class="container">
    <div class="row jusitfy-content-center">
        <div class="col-lg-10" >
            <h1>Your notes</h1>
            <?php


                while($row = mysqli_fetch_assoc($ress)){
            ?>
                    <div class="card-body" style="border: 1px solid; opacity: 0.75; float: left; width: 950px;" contenteditable="true" id="<?php echo $row['id_biljeska']?>">

                        <?php echo $row['opis']?>

                        </div>

                    <div id="msg<?php echo $row['id_biljeska']?>" class="msg"></div>
                    <input type="button" onclick="deleteData('<?php echo $row['id_biljeska']?>')" style="margin-left: 15px;" value="x" class="btn btn-danger"   >
                   <input type="button" id="submit-data" onclick="updateData('<?php echo $row['id_biljeska']?>')" value="&#10003;"  class="btn btn-primary"  >

                    <br/><br/>

                <?php } ?>

    </div>

</div>

    <script type="text/javascript">

        function updateData(id){
            let html=jQuery('#'+id).html();
            jQuery.ajax({
                url:'update.php',
                type:'post',
                data:'html='+html+'&id='+id,
                success:function(result){
                    jQuery('#msg'+id).html(result);
                }

            });
        }
        function deleteData(id){
            let html=jQuery('#'+id).html();
            jQuery.ajax({
                url:'delete.php',
                type:'post',
                data:'html='+html+'&id='+id,
                success:function(result){
                    jQuery('#msg'+id).html(result);
                }

            });
        }
        function colorOnBlur(){
            this.style.backgroundColor = '#f00';
        }

        var inputs = document.querySelectorAll('div');

        for (var i = 0, len = inputs.length; i < len; i++){
            inputs[i].addEventListener('blur', colorOnBlur);
        }
        $('form')
            .each(function(){
                $(this).data('serialized', $(this).serialize())
            })
            .on('change input', function(){
                $(this)
                    .find('input:submit, button:submit')
                    .attr('disabled', $(this).serialize() == $(this).data('serialized'))
                ;
            })
            .find('input:submit, button:submit')
            .attr('disabled', true)
        ;
    </script>

</body>




























